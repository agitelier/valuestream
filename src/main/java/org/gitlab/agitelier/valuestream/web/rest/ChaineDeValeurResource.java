package org.gitlab.agitelier.valuestream.web.rest;

import org.gitlab.agitelier.valuestream.domain.ChaineDeValeur;
import org.gitlab.agitelier.valuestream.repository.ChaineDeValeurRepository;
import org.gitlab.agitelier.valuestream.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.gitlab.agitelier.valuestream.domain.ChaineDeValeur}.
 */
@RestController
@RequestMapping("/api")
public class ChaineDeValeurResource {

    private final Logger log = LoggerFactory.getLogger(ChaineDeValeurResource.class);

    private static final String ENTITY_NAME = "chaineDeValeur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChaineDeValeurRepository chaineDeValeurRepository;

    public ChaineDeValeurResource(ChaineDeValeurRepository chaineDeValeurRepository) {
        this.chaineDeValeurRepository = chaineDeValeurRepository;
    }

    /**
     * {@code POST  /chaine-de-valeurs} : Create a new chaineDeValeur.
     *
     * @param chaineDeValeur the chaineDeValeur to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chaineDeValeur, or with status {@code 400 (Bad Request)} if the chaineDeValeur has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chaine-de-valeurs")
    public ResponseEntity<ChaineDeValeur> createChaineDeValeur(@RequestBody ChaineDeValeur chaineDeValeur) throws URISyntaxException {
        log.debug("REST request to save ChaineDeValeur : {}", chaineDeValeur);
        if (chaineDeValeur.getId() != null) {
            throw new BadRequestAlertException("A new chaineDeValeur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChaineDeValeur result = chaineDeValeurRepository.save(chaineDeValeur);
        return ResponseEntity.created(new URI("/api/chaine-de-valeurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /chaine-de-valeurs} : Updates an existing chaineDeValeur.
     *
     * @param chaineDeValeur the chaineDeValeur to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chaineDeValeur,
     * or with status {@code 400 (Bad Request)} if the chaineDeValeur is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chaineDeValeur couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chaine-de-valeurs")
    public ResponseEntity<ChaineDeValeur> updateChaineDeValeur(@RequestBody ChaineDeValeur chaineDeValeur) throws URISyntaxException {
        log.debug("REST request to update ChaineDeValeur : {}", chaineDeValeur);
        if (chaineDeValeur.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChaineDeValeur result = chaineDeValeurRepository.save(chaineDeValeur);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chaineDeValeur.getId()))
            .body(result);
    }

    /**
     * {@code GET  /chaine-de-valeurs} : get all the chaineDeValeurs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chaineDeValeurs in body.
     */
//    @GetMapping("/chaine-de-valeurs")
//    public List<ChaineDeValeur> getAllChaineDeValeurs() {
//        log.debug("REST request to get all ChaineDeValeurs");
//        return chaineDeValeurRepository.findAll();
//    }
    @GetMapping(path ="/chaine-de-valeurs")
    public List<ChaineDeValeur> getAllChaineDeValeurs(
        @RequestParam(required = false, name = "sousGroupe.equals") String sousGroupe){
        log.debug("REST request to get all ChaineDeValeurs");
        if (sousGroupe != null){
            return chaineDeValeurRepository.findBySousGroupe(sousGroupe);
        }
        else{
            return chaineDeValeurRepository.findAll();
        }
    }

    /**
     * {@code GET  /chaine-de-valeurs/:id} : get the "id" chaineDeValeur.
     *
     * @param id the id of the chaineDeValeur to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chaineDeValeur, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chaine-de-valeurs/{id}")
    public ResponseEntity<ChaineDeValeur> getChaineDeValeur(@PathVariable String id) {
        log.debug("REST request to get ChaineDeValeur : {}", id);
        Optional<ChaineDeValeur> chaineDeValeur = chaineDeValeurRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chaineDeValeur);
    }

    /**
     * {@code DELETE  /chaine-de-valeurs/:id} : delete the "id" chaineDeValeur.
     *
     * @param id the id of the chaineDeValeur to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chaine-de-valeurs/{id}")
    public ResponseEntity<Void> deleteChaineDeValeur(@PathVariable String id) {
        log.debug("REST request to delete ChaineDeValeur : {}", id);
        chaineDeValeurRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
