/**
 * Data Access Objects used by WebSocket services.
 */
package org.gitlab.agitelier.valuestream.web.websocket.dto;
