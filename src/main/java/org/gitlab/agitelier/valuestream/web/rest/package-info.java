/**
 * Spring MVC REST controllers.
 */
package org.gitlab.agitelier.valuestream.web.rest;
