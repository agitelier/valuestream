/**
 * WebSocket services, using Spring Websocket.
 */
package org.gitlab.agitelier.valuestream.web.websocket;
