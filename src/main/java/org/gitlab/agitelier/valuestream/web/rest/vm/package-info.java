/**
 * View Models used by Spring MVC REST controllers.
 */
package org.gitlab.agitelier.valuestream.web.rest.vm;
