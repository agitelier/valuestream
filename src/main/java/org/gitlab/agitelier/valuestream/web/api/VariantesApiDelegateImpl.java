package org.gitlab.agitelier.valuestream.web.api;

import io.github.jhipster.web.util.PaginationUtil;
import org.gitlab.agitelier.valuestream.web.api.model.Variant;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VariantesApiDelegateImpl implements VariantesApiDelegate {

    /**
     * Affiche la liste des variantes sous format JSON
     *
     * @return ResponseEntity
     */
    @GetMapping("/variantes")
    public ResponseEntity<List<Variant>> getWorkshopVariants() {
        List<Variant> variants = new ArrayList<>();
        Variant starbucks = creerVariant("st11", "Starbucks", "Starbucks Corporation est une chaîne de cafés américaine fondée en 1971.");
        Variant timHortons = creerVariant("th11", "Tim Hortons", "Tim Hortons Inc. est une chaîne canadienne de restaurants fondée en 1964.");

        variants.add(starbucks);
        variants.add(timHortons);

        Pageable pageable = PageRequest.of(0, variants.size());
        Page<Variant> page = new PageImpl<>(variants, pageable, variants.size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(variants);
    }

    /**
     * Crée une variante
     *
     * @param id
     * @param title
     * @param description
     * @return variant
     */
    private Variant creerVariant(String id, String title, String description) {
        Variant variant = new Variant();

        variant.setId(id);
        variant.setTitle(title);
        variant.setDescription(description);

        return variant;
    }

    /**
     * Affiche une variante selon l'id fourni en paramètre
     *
     * @param id Numeric ID of the variant to get (required)
     * @return
     */
    @GetMapping("/variantes/{id}")
    public ResponseEntity<Variant> getWorkshopVariant(@PathVariable String id) {
        Variant variant = new Variant();

        variant.setId(id);
        variant.setTitle("Un nom de variante");
        variant.setDescription("Description de variante");

        return new ResponseEntity<Variant>(variant, HttpStatus.OK);
    }

}
