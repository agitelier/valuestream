package org.gitlab.agitelier.valuestream.repository;

import org.gitlab.agitelier.valuestream.domain.ChaineDeValeur;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the ChaineDeValeur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChaineDeValeurRepository extends MongoRepository<ChaineDeValeur, String> {
    List<ChaineDeValeur> findBySousGroupe(String sousGroupe);
}
