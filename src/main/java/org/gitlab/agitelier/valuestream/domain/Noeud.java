package org.gitlab.agitelier.valuestream.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

public class Noeud {

    private String id;
    private String type;
    private String valeur;
    private String couleur;
    private double largeur;
    private double hauteur;
    private String strokeColor;
    private int strokeWeight;
    private String shape;
    private double x;
    private double y;

    public Noeud() {
        this.id = "";
        this.type= "";
        this.valeur = "";
        this.couleur = "";
        this.x = 0.0;
        this.y = 0.0;
    }

    public Noeud(String id, String type, String valeur, String couleur, double largeur, double hauteur, double x, double y) {
        this.id = id;
        this.id = type;
        this.valeur = valeur;
        this.couleur = couleur;
        this.largeur = largeur;
        this.hauteur = hauteur;
        this.x = x;
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() { return type; }

    public void setType(String type) {
        this.type = type;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public double getLargeur() {
        return largeur;
    }

    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }

    public double getHauteur() {
        return hauteur;
    }

    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
