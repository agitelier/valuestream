package org.gitlab.agitelier.valuestream.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A ChaineDeValeur.
 */

@Document(collection = "chaine_de_valeur")
public class ChaineDeValeur implements Serializable {

    private static final long serialVersionUID = 1L;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    @Id
    private String id;

    @Field("groupe")
    private String groupe;

    @Field("sous_groupe")
    private String sousGroupe;

    @Field("chevrons")
    private List<Noeud> noeuds = new ArrayList<>();

    @Field("liens")
    private List<Lien> liens = new ArrayList<>();


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupe() {
        return groupe;
    }

    public ChaineDeValeur groupe(String groupe) {
        this.groupe = groupe;
        return this;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getSousGroupe() {
        return sousGroupe;
    }

    public ChaineDeValeur sousGroupe(String sousGroupe) {
        this.sousGroupe = sousGroupe;
        return this;
    }

    public void setSousGroupe(String sousGroupe) {
        this.sousGroupe = sousGroupe;
    }


    public List<Noeud> getNoeuds() {
        return noeuds;
    }

    public void setNoeuds(List<Noeud> noeuds) {
        this.noeuds = noeuds;
    }

    public ChaineDeValeur noeuds(List<Noeud> noeuds) {
        this.noeuds = noeuds;
        return this;
    }

    public List<Lien> getLiens() {
        return liens;
    }

    public void setLiens(List<Lien> liens) {
        this.liens = liens;
    }

    public ChaineDeValeur liens(List<Lien> liens) {
        this.liens = liens;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChaineDeValeur that = (ChaineDeValeur) o;
        return id.equals(that.id) && groupe.equals(that.groupe) && sousGroupe.equals(that.sousGroupe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, groupe, sousGroupe);
    }

    @Override
    public String toString() {
        return "ChaineDeValeur{" +
            "id='" + id + '\'' +
            ", groupe='" + groupe + '\'' +
            ", sousGroupe='" + sousGroupe + '\'' +
            ", noeuds=" + noeuds +
            ", liens=" + liens +
            '}';
    }
}
