package org.gitlab.agitelier.valuestream.domain;

public class Lien {

    private String id;
    private String source;
    private String destination;
    private double x;
    private double y;

    public Lien() {
        this.id = "";
        this.source = "";
        this.destination = "";
        this.x = 0.0;
        this.y = 0.0;
    }

    public Lien(String id, String source, String destination, double x, double y) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.x = x;
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) { this.source = source; }

    public String getDestination() { return destination; }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

/*
  "links": [
    {
      "id": "1787b18882d95d8a",
      "source": "1787b1873058df37",
      "destination": "1787b181ef73be29",
      "point": {
        "x": 402.5,
        "y": 162.05655787521476
      }
    }
  ]
 */

}
