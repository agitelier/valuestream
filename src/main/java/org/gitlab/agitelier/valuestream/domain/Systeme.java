package org.gitlab.agitelier.valuestream.domain;

public class Systeme {
    private String valeur;
    private String couleur;
    private int width;
    private int height;
    private String strokeColor;
    private int strokeWeight;
    private String shape;
    private double x;
    private double y;


    public Systeme(String valeur, double x, double y) {
        this.valeur = valeur;
        this.x = x;
        this.y = y;
    }
    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}


/*
{
  "width": 1200,
  "height": 370,
  "background": "#eee",
  "nodes": [
    {
      "id": "1787b181ef73be29",
      "content": {
        "text": "chevron001",
        "color": "#21AB0A"
      },
      "width": 150,
      "height": 60,
      "stroke": "black",
      "strokeWeight": "1",
      "shape": "polygon",
      "point": {
        "x": 118,
        "y": 109.05655787521476
      }
    },
    {
      "id": "1787b1873058df37",
      "content": {
        "text": "Système001",
        "color": "#DDE40B"
      },
      "width": 180,
      "height": 60,
      "point": {
        "x": 529,
        "y": 148.05655787521476
      },
      "shape": "rectangle",
      "stroke": "black",
      "strokeWeight": "1"
    }
  ],
  "links": [
    {
      "id": "1787b18882d95d8a",
      "source": "1787b1873058df37",
      "destination": "1787b181ef73be29",
      "point": {
        "x": 402.5,
        "y": 162.05655787521476
      }
    }
  ],
  "showGrid": false
}

 */
