/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package org.gitlab.agitelier.valuestream.service.mapper;
