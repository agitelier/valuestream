import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IChaineDeValeur } from '@/shared/model/chaine-de-valeur.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import ChaineDeValeurService from './chaine-de-valeur.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class ChaineDeValeur extends mixins(AlertMixin) {
  @Inject('chaineDeValeurService') private chaineDeValeurService: () => ChaineDeValeurService;
  private removeId: string = null;
  public sousGroupe = 'default';

  public chaineDeValeurs: IChaineDeValeur[] = [];

  public isFetching = false;

  public mounted(): void {
    if (this.$route.params.sousGroupeId) {
      this.setSousGroupe(this.$route.params.sousGroupeId);
    }
    this.retrieveAllChaineDeValeurs();
  }

  public setSousGroupe(sousGroupe) {
    this.sousGroupe = sousGroupe;
  }

  public clear(): void {
    this.retrieveAllChaineDeValeurs();
  }

  public retrieveAllChaineDeValeurs(): void {
    this.isFetching = true;

    this.chaineDeValeurService()
      .retrieve(this.sousGroupe)
      .then(
        res => {
          this.chaineDeValeurs = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IChaineDeValeur): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeChaineDeValeur(): void {
    this.chaineDeValeurService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('valuestreamApp.chaineDeValeur.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllChaineDeValeurs();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
