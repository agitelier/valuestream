import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IChaineDeValeur, ChaineDeValeur } from '@/shared/model/chaine-de-valeur.model';
import ChaineDeValeurService from './chaine-de-valeur.service';

import { DiagramEditor } from './index.js';
import data from './data.json';
import Diagram from '@/entities/chaine-de-valeur/Diagram.vue';

const validations: any = {
  chaineDeValeur: {
    groupe: {},
    sousGroupe: {},
    noeuds: {},
    liens: {},
  },
};

@Component({
  components: {
    DiagramEditor,
  },
  validations,
})
export default class ChaineDeValeurUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('chaineDeValeurService') private chaineDeValeurService: () => ChaineDeValeurService;
  public chaineDeValeur: IChaineDeValeur = new ChaineDeValeur();
  public isSaving = false;
  public currentLanguage = '';
  private graph: any;

  data() {
    return {
      demo: 'default',
      graph: {},
    };
  }

  beforeUpdate() {
    this.graph = data;
    this.setNodes();
    this.setLinks();
  }

  setNodes() {
    this.graph.nodes = [];
    this.$v.chaineDeValeur.noeuds.$model.map(noeud => {
      this.graph.nodes.push({
        id: noeud.id,
        type: noeud.type,
        content: {
          text: noeud.valeur,
          color: noeud.couleur,
          opacity: noeud.opacity,
        },
        width: noeud.largeur,
        height: noeud.hauteur,
        stroke: '#292929',
        strokeWeight: '1',
        // shape: 'polygon',
        point: {
          x: noeud.x,
          y: noeud.y,
        },
        isCollided: false,
      });
    });
  }

  setLinks() {
    this.graph.links = [];
    this.$v.chaineDeValeur.liens.$model.map(lien => {
      this.graph.links.push({
        id: lien.id,
        source: lien.source,
        destination: lien.destination,
        point: {
          x: lien.x,
          y: lien.y,
        },
      });
    });
  }

  refresh() {
    this.graph = data;
  }

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.chaineDeValeurId) {
        vm.retrieveChaineDeValeur(to.params.chaineDeValeurId);
      }
    });
  }

  public mounted(): void {
    if (this.$route.params.sousGroupeId) {
      this.setSousGroupe(this.$route.params.sousGroupeId);
    }
  }

  public setSousGroupe(sousGroupe) {
    this.chaineDeValeur.sousGroupe = sousGroupe;
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    // Ramener les nodes à jours dans l'objet noeuds
    this.getNodes();
    // Ramener les links à jours dans l'objet liens
    this.getLinks();
    if (this.chaineDeValeur.id) {
      this.chaineDeValeurService()
        .update(this.chaineDeValeur)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('valuestreamApp.chaineDeValeur.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.chaineDeValeurService()
        .create(this.chaineDeValeur)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('valuestreamApp.chaineDeValeur.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  getNodes() {
    this.$v.chaineDeValeur.noeuds.$model = [];
    this.graph.nodes.map(node => {
      this.$v.chaineDeValeur.noeuds.$model.push({
        id: node.id,
        type: node.type,
        valeur: node.content.text,
        couleur: node.content.color,
        opacity: node.content.opacity,
        largeur: node.width,
        hauteur: node.height,
        x: node.point.x,
        y: node.point.y,
      });
    });
  }

  getLinks() {
    this.$v.chaineDeValeur.liens.$model = [];
    this.graph.links.map(link => {
      this.$v.chaineDeValeur.liens.$model.push({
        id: link.id,
        source: link.source,
        destination: link.destination,
        x: link.point.x,
        y: link.point.y,
      });
    });
  }

  public retrieveChaineDeValeur(chaineDeValeurId): void {
    this.chaineDeValeurService()
      .find(chaineDeValeurId)
      .then(res => {
        this.chaineDeValeur = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }
}
