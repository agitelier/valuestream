import { Component, Vue, Inject } from 'vue-property-decorator';

import { IChaineDeValeur } from '@/shared/model/chaine-de-valeur.model';
import ChaineDeValeurService from './chaine-de-valeur.service';

@Component
export default class ChaineDeValeurDetails extends Vue {
  @Inject('chaineDeValeurService') private chaineDeValeurService: () => ChaineDeValeurService;
  public chaineDeValeur: IChaineDeValeur = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.chaineDeValeurId) {
        vm.retrieveChaineDeValeur(to.params.chaineDeValeurId);
      }
    });
  }

  public retrieveChaineDeValeur(chaineDeValeurId) {
    this.chaineDeValeurService()
      .find(chaineDeValeurId)
      .then(res => {
        this.chaineDeValeur = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
