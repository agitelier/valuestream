import axios from 'axios';

import { IChaineDeValeur } from '@/shared/model/chaine-de-valeur.model';

const baseApiUrl = 'api/chaine-de-valeurs';

export default class ChaineDeValeurService {
  public find(id: string): Promise<IChaineDeValeur> {
    return new Promise<IChaineDeValeur>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(sousGroupe: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?sousGroupe.equals=${sousGroupe}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IChaineDeValeur): Promise<IChaineDeValeur> {
    return new Promise<IChaineDeValeur>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IChaineDeValeur): Promise<IChaineDeValeur> {
    return new Promise<IChaineDeValeur>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
