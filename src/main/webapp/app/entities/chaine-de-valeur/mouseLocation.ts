import { Component, Vue } from 'vue-property-decorator';
import { DiagramEditor } from './index.js';

@Component({
  components: {
    DiagramEditor,
  },
})
export default class App extends Vue {
  getLocation(e) {
    /* Returns the mouse cursor position or touch position */
    let x = 0;
    let y = 0;
    if (e.touches) {
      /* For touch */
      x = e.touches[0].pageX;
      y = e.touches[0].pageY;
    } else {
      /* For mouse */
      x = e.pageX;
      y = e.pageY;
    }
    return [x, y];
  }
}
