export interface IChaineDeValeur {
  id?: string;
  groupe?: string;
  sousGroupe?: string;
}

export class ChaineDeValeur implements IChaineDeValeur {
  constructor(public id?: string, public groupe?: string, public sousGroupe?: string) {}
}
