import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const ChaineDeValeur = () => import('@/entities/chaine-de-valeur/chaine-de-valeur.vue');
// prettier-ignore
const ChaineDeValeurUpdate = () => import('@/entities/chaine-de-valeur/chaine-de-valeur-update.vue');
// prettier-ignore
const ChaineDeValeurDetails = () => import('@/entities/chaine-de-valeur/chaine-de-valeur-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/:sousGroupeId',
    name: 'ChaineDeValeur',
    component: ChaineDeValeur,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/:sousGroupeId/new',
    name: 'ChaineDeValeurCreate',
    component: ChaineDeValeurUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/:chaineDeValeurId/edit',
    name: 'ChaineDeValeurEdit',
    component: ChaineDeValeurUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/:chaineDeValeurId/view',
    name: 'ChaineDeValeurView',
    component: ChaineDeValeurDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
