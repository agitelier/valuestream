/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ChaineDeValeurDetailComponent from '@/entities/chaine-de-valeur/chaine-de-valeur-details.vue';
import ChaineDeValeurClass from '@/entities/chaine-de-valeur/chaine-de-valeur-details.component';
import ChaineDeValeurService from '@/entities/chaine-de-valeur/chaine-de-valeur.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ChaineDeValeur Management Detail Component', () => {
    let wrapper: Wrapper<ChaineDeValeurClass>;
    let comp: ChaineDeValeurClass;
    let chaineDeValeurServiceStub: SinonStubbedInstance<ChaineDeValeurService>;

    beforeEach(() => {
      chaineDeValeurServiceStub = sinon.createStubInstance<ChaineDeValeurService>(ChaineDeValeurService);

      wrapper = shallowMount<ChaineDeValeurClass>(ChaineDeValeurDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { chaineDeValeurService: () => chaineDeValeurServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundChaineDeValeur = { id: '123' };
        chaineDeValeurServiceStub.find.resolves(foundChaineDeValeur);

        // WHEN
        comp.retrieveChaineDeValeur('123');
        await comp.$nextTick();

        // THEN
        expect(comp.chaineDeValeur).toBe(foundChaineDeValeur);
      });
    });
  });
});
