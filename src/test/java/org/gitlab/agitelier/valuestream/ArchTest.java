package org.gitlab.agitelier.valuestream;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("org.gitlab.agitelier.valuestream");

        noClasses()
            .that()
                .resideInAnyPackage("org.gitlab.agitelier.valuestream.service..")
            .or()
                .resideInAnyPackage("org.gitlab.agitelier.valuestream.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..org.gitlab.agitelier.valuestream.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
