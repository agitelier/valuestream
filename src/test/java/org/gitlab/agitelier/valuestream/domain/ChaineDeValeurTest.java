package org.gitlab.agitelier.valuestream.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.gitlab.agitelier.valuestream.web.rest.TestUtil;

public class ChaineDeValeurTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChaineDeValeur.class);
        ChaineDeValeur chaineDeValeur1 = new ChaineDeValeur();
        chaineDeValeur1.setId("id1");
        ChaineDeValeur chaineDeValeur2 = new ChaineDeValeur();
        chaineDeValeur2.setId(chaineDeValeur1.getId());
        assertThat(chaineDeValeur1).isEqualTo(chaineDeValeur2);
        chaineDeValeur2.setId("id2");
        assertThat(chaineDeValeur1).isNotEqualTo(chaineDeValeur2);
        chaineDeValeur1.setId(null);
        assertThat(chaineDeValeur1).isNotEqualTo(chaineDeValeur2);
    }
}
