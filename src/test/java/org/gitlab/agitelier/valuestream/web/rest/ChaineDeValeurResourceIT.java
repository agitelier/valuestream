package org.gitlab.agitelier.valuestream.web.rest;

import org.gitlab.agitelier.valuestream.ValuestreamApp;
import org.gitlab.agitelier.valuestream.config.TestSecurityConfiguration;
import org.gitlab.agitelier.valuestream.domain.ChaineDeValeur;
import org.gitlab.agitelier.valuestream.repository.ChaineDeValeurRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChaineDeValeurResource} REST controller.
 */
@SpringBootTest(classes = { ValuestreamApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class ChaineDeValeurResourceIT {

    private static final String DEFAULT_GROUPE = "AAAAAAAAAA";
    private static final String UPDATED_GROUPE = "BBBBBBBBBB";

    private static final String DEFAULT_SOUS_GROUPE = "AAAAAAAAAA";
    private static final String UPDATED_SOUS_GROUPE = "BBBBBBBBBB";

    @Autowired
    private ChaineDeValeurRepository chaineDeValeurRepository;

    @Autowired
    private MockMvc restChaineDeValeurMockMvc;

    private ChaineDeValeur chaineDeValeur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChaineDeValeur createEntity() {
        ChaineDeValeur chaineDeValeur = new ChaineDeValeur()
            .groupe(DEFAULT_GROUPE)
            .sousGroupe(DEFAULT_SOUS_GROUPE);
        return chaineDeValeur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChaineDeValeur createUpdatedEntity() {
        ChaineDeValeur chaineDeValeur = new ChaineDeValeur()
            .groupe(UPDATED_GROUPE)
            .sousGroupe(UPDATED_SOUS_GROUPE);
        return chaineDeValeur;
    }

    @BeforeEach
    public void initTest() {
        chaineDeValeurRepository.deleteAll();
        chaineDeValeur = createEntity();
    }

    @Test
    public void createChaineDeValeur() throws Exception {
        int databaseSizeBeforeCreate = chaineDeValeurRepository.findAll().size();
        // Create the ChaineDeValeur
        restChaineDeValeurMockMvc.perform(post("/api/chaine-de-valeurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chaineDeValeur)))
            .andExpect(status().isCreated());

        // Validate the ChaineDeValeur in the database
        List<ChaineDeValeur> chaineDeValeurList = chaineDeValeurRepository.findAll();
        assertThat(chaineDeValeurList).hasSize(databaseSizeBeforeCreate + 1);
        ChaineDeValeur testChaineDeValeur = chaineDeValeurList.get(chaineDeValeurList.size() - 1);
        assertThat(testChaineDeValeur.getGroupe()).isEqualTo(DEFAULT_GROUPE);
        assertThat(testChaineDeValeur.getSousGroupe()).isEqualTo(DEFAULT_SOUS_GROUPE);
    }

    @Test
    public void createChaineDeValeurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chaineDeValeurRepository.findAll().size();

        // Create the ChaineDeValeur with an existing ID
        chaineDeValeur.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restChaineDeValeurMockMvc.perform(post("/api/chaine-de-valeurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chaineDeValeur)))
            .andExpect(status().isBadRequest());

        // Validate the ChaineDeValeur in the database
        List<ChaineDeValeur> chaineDeValeurList = chaineDeValeurRepository.findAll();
        assertThat(chaineDeValeurList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllChaineDeValeurs() throws Exception {
        // Initialize the database
        chaineDeValeurRepository.save(chaineDeValeur);

        // Get all the chaineDeValeurList
        restChaineDeValeurMockMvc.perform(get("/api/chaine-de-valeurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chaineDeValeur.getId())))
            .andExpect(jsonPath("$.[*].groupe").value(hasItem(DEFAULT_GROUPE)))
            .andExpect(jsonPath("$.[*].sousGroupe").value(hasItem(DEFAULT_SOUS_GROUPE)));
    }

    @Test
    public void getChaineDeValeur() throws Exception {
        // Initialize the database
        chaineDeValeurRepository.save(chaineDeValeur);

        // Get the chaineDeValeur
        restChaineDeValeurMockMvc.perform(get("/api/chaine-de-valeurs/{id}", chaineDeValeur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chaineDeValeur.getId()))
            .andExpect(jsonPath("$.groupe").value(DEFAULT_GROUPE))
            .andExpect(jsonPath("$.sousGroupe").value(DEFAULT_SOUS_GROUPE));
    }
    @Test
    public void getNonExistingChaineDeValeur() throws Exception {
        // Get the chaineDeValeur
        restChaineDeValeurMockMvc.perform(get("/api/chaine-de-valeurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateChaineDeValeur() throws Exception {
        // Initialize the database
        chaineDeValeurRepository.save(chaineDeValeur);

        int databaseSizeBeforeUpdate = chaineDeValeurRepository.findAll().size();

        // Update the chaineDeValeur
        ChaineDeValeur updatedChaineDeValeur = chaineDeValeurRepository.findById(chaineDeValeur.getId()).get();
        updatedChaineDeValeur
            .groupe(UPDATED_GROUPE)
            .sousGroupe(UPDATED_SOUS_GROUPE);

        restChaineDeValeurMockMvc.perform(put("/api/chaine-de-valeurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedChaineDeValeur)))
            .andExpect(status().isOk());

        // Validate the ChaineDeValeur in the database
        List<ChaineDeValeur> chaineDeValeurList = chaineDeValeurRepository.findAll();
        assertThat(chaineDeValeurList).hasSize(databaseSizeBeforeUpdate);
        ChaineDeValeur testChaineDeValeur = chaineDeValeurList.get(chaineDeValeurList.size() - 1);
        assertThat(testChaineDeValeur.getGroupe()).isEqualTo(UPDATED_GROUPE);
        assertThat(testChaineDeValeur.getSousGroupe()).isEqualTo(UPDATED_SOUS_GROUPE);
    }

    @Test
    public void updateNonExistingChaineDeValeur() throws Exception {
        int databaseSizeBeforeUpdate = chaineDeValeurRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChaineDeValeurMockMvc.perform(put("/api/chaine-de-valeurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chaineDeValeur)))
            .andExpect(status().isBadRequest());

        // Validate the ChaineDeValeur in the database
        List<ChaineDeValeur> chaineDeValeurList = chaineDeValeurRepository.findAll();
        assertThat(chaineDeValeurList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteChaineDeValeur() throws Exception {
        // Initialize the database
        chaineDeValeurRepository.save(chaineDeValeur);

        int databaseSizeBeforeDelete = chaineDeValeurRepository.findAll().size();

        // Delete the chaineDeValeur
        restChaineDeValeurMockMvc.perform(delete("/api/chaine-de-valeurs/{id}", chaineDeValeur.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChaineDeValeur> chaineDeValeurList = chaineDeValeurRepository.findAll();
        assertThat(chaineDeValeurList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
